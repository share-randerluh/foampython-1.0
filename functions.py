"""
/*---------------------------------------------------------------------------*\
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------

Author
    Robert Anderluh, 2021
    
Description
    General useful functions
    
\*---------------------------------------------------------------------------*/
"""

import os
import psutil
import numpy as np
from src.OpenFOAM.fields.include import *

def printMemoryUsageInMB():
    print("\nMemory usage is: " + str(psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2) + ' MB\n')

def startStatement(solverName, caseFolder, date):
    print("""-------------------------------------------------------------------------------
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------""")
    
    print("Solver           : " + solverName)
    print("Case             : " + caseFolder)
    print("Date and time    : " + date)
    print("\n")




    
