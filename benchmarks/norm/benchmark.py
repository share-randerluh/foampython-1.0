import timeit

nRepeats = 1000000

t1 = timeit.timeit("np.linalg.norm(C1 - C2)", setup = "from scipy.spatial import distance ; import math ; import numpy as np ; C1 = np.array([1,0,0]) ; C2 = np.array([0,1,0])", number = nRepeats)

t2 = timeit.timeit("distance.euclidean(C1,C2)", setup = "from scipy.spatial import distance ; import math ; import numpy as np ; C1 = np.array([1,0,0]) ; C2 = np.array([0,1,0])", number = nRepeats)

t3 = timeit.timeit("math.dist(C1,C2)", setup = "from scipy.spatial import distance ; import math ; import numpy as np ; C1 = np.array([1,0,0]) ; C2 = np.array([0,1,0])", number = nRepeats)

print("Numpy : " + str(t1) + " s")
print("scipy : " + str(t2) + " s")
print("Math : " + str(t3) + " s")
