def factorial(x, nRepeats):
    x = int(x)
    nRepeats = int(nRepeats)
    
    for j in range(nRepeats):
        
        f = 1
        
        for i in range(x):
            f *= (x - i)
    
    return f

# print("{:.2e}".format(factorial(1e3)))

# no1 = factorial(5)
# print(format(no1,'.3E'))
