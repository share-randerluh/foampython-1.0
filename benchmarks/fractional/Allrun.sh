g++ -O0 factcpp.C -o cppfactO0
g++ -O1 factcpp.C -o cppfactO1
g++ -O2 factcpp.C -o cppfactO2
g++ -O3 factcpp.C -o cppfactO3

cd cythonFolder
sh cythonize.sh
cd ..

python3 benchmark.py
