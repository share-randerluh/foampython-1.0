from timeit import timeit

nRepeats = 10000000

t1 = timeit( "factorial(10, nRepeats)",
    setup ="from fact import factorial; nRepeats = "+str(nRepeats),
    number = 1,
    )

t2 = timeit( "cfactorial(10, nRepeats)",
    setup ="from cythonFolder.cythonFolder.cfact import cfactorial ; nRepeats = "+str(nRepeats),
    number = 1,
    )

t3 = timeit( "npfactorial(10, nRepeats)",
    setup ="from factnp import npfactorial; nRepeats = "+str(nRepeats),
    number = 1,
    )

t4 = timeit( 'os.system("./cppfactO0 10 " + str(nRepeats))',
    setup ="import os ; nRepeats = " + str(nRepeats),
    number = 1,
    )

t5 = timeit( 'os.system("./cppfactO1 10 " + str(nRepeats))',
    setup ="import os ; nRepeats = " + str(nRepeats),
    number = 1,
    )

t6 = timeit( 'os.system("./cppfactO2 10 " + str(nRepeats))',
    setup ="import os ; nRepeats = " + str(nRepeats),
    number = 1,
    )

t7 = timeit( 'os.system("./cppfactO3 10 " + str(nRepeats))',
    setup ="import os ; nRepeats = " + str(nRepeats),
    number = 1,
    )

print(f"Python: {t1:.3f}")
print(f"Cython: {t2:.3f}")
print(f"Numpy: {t3:.3f}")
print(f"C++ O0: {t4:.3f}")
print(f"C++ O1: {t5:.3f}")
print(f"C++ O2: {t6:.3f}")
print(f"C++ O3: {t7:.3f}")

print(f"Cython is {t1 / t2:.3f}x faster than pure Python")
