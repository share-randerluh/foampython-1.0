##cython: language_level=3, boundscheck=False, wraparound=False, initializedcheck=False, extra_compile_args=aaa
## distutils: extra_compile_args=['-O3', '-mavx', '-ffast-math']
## distutils: extra_link_args=['-O3', '-mavx', '-ffast-math']

#cython: language_level=3

cpdef int cfactorial(int x, int nRepeats):
    cdef int f = 1
    cdef int i
    cdef int j
    
    for j in range(nRepeats):
        
        f = 1
        
        for i in range(x):
            f *= (x - i)
        
    return f
    
