import numpy

def factorial(x, nRepeats):
    
    results = numpy.empty(x, dtype=int)
    
    for j in range(nRepeats):
        
        results[0] = 1
        
        for i in range(1, x):
            
            results[i] = (i+1) * results[i-1]
    
    print("Python result = ", results)
    
    return x
