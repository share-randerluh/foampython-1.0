from timeit import timeit

nRepeats = 10000000

t1 = timeit( "factorial(10, nRepeats)",
    setup ="from fact import factorial; nRepeats = "+str(nRepeats),
    number = 1,
    )

t2 = timeit( "cfactorial1(10, nRepeats)",
    setup ="from cythonFolder.cythonFolder.cfact import cfactorial1 ; nRepeats = "+str(nRepeats),
    number = 1,
    )

t3 = timeit( "cfactorial2(10, nRepeats)",
    setup ="from cythonFolder.cythonFolder.cfact import cfactorial2 ; nRepeats = "+str(nRepeats),
    number = 1,
    )

t4 = timeit( "cfactorial3(10, nRepeats)",
    setup ="from cythonFolder.cythonFolder.cfact import cfactorial3 ; nRepeats = "+str(nRepeats),
    number = 1,
    )

t5 = timeit( "factorial(10, nRepeats)",
    setup ="from cythonFolder.cythonFolder.cfact import factorial ; nRepeats = "+str(nRepeats),
    number = 1,
    )

print(f"Python: {t1:.3f}")
print(f"Cython 1: {t2:.3f} - with type declarations")
print(f"Cython 2: {t3:.3f} - with array element type declarations and dimensions")
print(f"Cython 3: {t4:.3f} - remove bound checking and wrapping around")
print(f"Cython 4 - Naive: {t5:.3f} - no cython syntax")
