#cython: language_level=3

import numpy as np
cimport numpy as np

np.import_array()

#~ Simple Cython
cpdef int cfactorial1(int x, int nRepeats):
    
    cdef int i
    cdef int j
    cdef np.ndarray results = np.empty(x, dtype=np.int)
    
    for j in range(nRepeats):
        
        results[0] = 1
        
        for i in range(1, x):
            
            results[i] = (i+1) * results[i-1]
    
    print("Cython result = ", results)
    
    return x
    
    
#~ Array improvement Cython
cpdef int cfactorial2(int x, int nRepeats):
    
    cdef int i
    cdef int j
    cdef np.ndarray[np.int_t, ndim=1] results = np.empty(x, dtype=np.int)
    
    for j in range(nRepeats):
        
        results[0] = 1
        
        for i in range(1, x):
            
            results[i] = (i+1) * results[i-1]
    
    print("Cython result = ", results)
    
    return x


#~ Further array improvement Cython
cimport cython
@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cpdef int cfactorial3(int x, int nRepeats):
    
    cdef int i
    cdef int j
    cdef np.ndarray[np.int_t, ndim=1] results = np.empty(x, dtype=np.int)
    
    for j in range(nRepeats):
        
        results[0] = 1
        
        for i in range(1, x):
            
            results[i] = (i+1) * results[i-1]
    
    print("Cython result = ", results)
    
    return x

def factorial(x, nRepeats):
    
    results = np.empty(x, dtype=int)
    
    for j in range(nRepeats):
        
        results[0] = 1
        
        for i in range(1, x):
            
            results[i] = (i+1) * results[i-1]
    
    print("Python result = ", results)
    
    return x
