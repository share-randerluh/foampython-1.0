# Case setup file for simpleFoam.py

"""--------------------------------------------------------------------------"""

# 2D cavity case
boundaryDefinition_U = {
'walls': ( 'fixedValue'              , np.array([0.0, 0.0, 0.0])), 
'inlet': ( 'fixedValue'              , np.array([0.2, 0.0, 0.0])),
'outlet': ( 'fixedGradient'          , np.array([0.0, 0.0, 0.0])),
'frontAndBack': ( 'empty'            , None   )
}

boundaryDefinition_p = {
'walls': ( 'fixedGradient'         , np.array([0.0])),
'inlet': ( 'fixedGradient'         , np.array([0.0])),
'outlet': ( 'fixedValue'           , np.array([100.0])),
'frontAndBack': ( 'empty'          , None)
}


"""--------------------------------------------------------------------------"""

internalField_U = np.array([0.0, 0.0, 0.0])
internalField_p = np.array([100.0])

# Kinematic viscosity
nu = 0.01

# Time control
startTime = 0.0

writeTime = 20

deltaT = 1

endTime = 200

# Finite volume schemes

ddtScheme = "steadyState"

laplacianScheme = "linearOrthogonal"

divUScheme = "upwind"

gradPScheme = "linear"

divHbyAScheme = "linear"

# Update case setup if it is modified
runTimeModifiable = True

# fvSolution parameters
fvSolution_U = {
'solver'        :    'GaussSeidel',
# 'solver'        :    'PointJacobi',
# 'minIter'        :    0,
# 'maxIter'        :    10,
'tolerance'        :    1e-6,
'relTol'        :    0.01,
'impUR'            :    0.7
}

fvSolution_p = {
'solver'        :    'GaussSeidel',
# 'solver'        :    'PointJacobi',
# 'minIter'        :    0,
# 'maxIter'        :    1000,
'tolerance'        :    1e-5,
'relTol'        :    0.1,
'expUR'            :    0.3,
}
