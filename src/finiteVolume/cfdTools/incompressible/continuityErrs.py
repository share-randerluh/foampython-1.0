"""
/*---------------------------------------------------------------------------*\
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------

Author
    Robert Anderluh, 2021
    
Description
    Code for continuity error-checking
    
\*---------------------------------------------------------------------------*/
"""

contErr = volScalarField.div(phi)
# contErr.updateFieldName("contErr")
# contErr.write(round(time, 10))

sumLocalContErr = deltaT * (contErr.mag()).weightedAverage(mesh.geometryData.V_)[0]

globalContErr = deltaT * (contErr).weightedAverage(mesh.geometryData.V_)[0]

cumulativeContErr += globalContErr

print("Time step continuity errors:" +                                          \
      " sum local = " + str(sumLocalContErr) +                                  \
      ", global = " + str(globalContErr) +                                      \
      ", cumulative = " + str(cumulativeContErr))
