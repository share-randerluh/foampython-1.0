"""
/*---------------------------------------------------------------------------*\
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------

Author
    Robert Anderluh, 2021
    
Description
    Include file for the implemented solvers
    
\*---------------------------------------------------------------------------*/
"""

from src.finiteVolume.fvSolution.solvers.GaussSeidel.GaussSeidel import *
from src.finiteVolume.fvSolution.solvers.PointJacobi.PointJacobi import *

"""
// ************************************************************************* //
"""
