"""
/*---------------------------------------------------------------------------*\
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------

Author
    Robert Anderluh, 2021
    
Description
    Setup file for Cythonizing the fvSolution class
    
\*---------------------------------------------------------------------------*/
"""

fileName = "fvSolution"

import os
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy

directives = {'language_level': 3,}
# # For profiling:
# directives = {'language_level': 3, 'profile' : True, 'linetrace' : True}

currDir = os.path.dirname(os.path.abspath(__file__))

setup(ext_modules = cythonize([Extension(fileName, [currDir + '/' + fileName + '.pyx'], include_dirs = [numpy.get_include()])], compiler_directives=directives))

"""
// ************************************************************************* //
"""
