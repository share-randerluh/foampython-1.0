"""
/*---------------------------------------------------------------------------*\
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------

Author
    Robert Anderluh, 2021
    
Description
    Boundary contributions for the fvm laplacian operator, pure Python.
    
\*---------------------------------------------------------------------------*/
"""

import numpy as np
import math

class laplacianBoundaryContributions:
    
    class boundaryContributions:
        
        class scalarGamma:
        
            def fixedValue(mesh, psi, boundaryName, source, diag, gamma):
                
                nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
                startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
                
                # Face area magnitudes
                magSf = mesh.geometryData.magSf_
                # Cell centres
                C = mesh.geometryData.C_
                # Face centres
                Cf = mesh.geometryData.Cf_
                # Values of psi on the boundary
                psiBoundaryValues = psi.data.boundaryValues_
                
                noComponents = psi.noComponents_
                
                # Number of internal faces
                nInternalFaces = np.size(mesh.connectivityData.neighbour_)
                
                for cmpt in range(noComponents):
                
                    for faceIndex in range(startFace, startFace + nFaces):
                        
                        boundaryCellIndex = mesh.connectivityData.owner_[faceIndex]
                               
                        boundaryIndex = faceIndex - nInternalFaces
                        
                        # ap = - gamma * |Sf| / |db|
                        diag[cmpt][boundaryCellIndex] +=                        \
                            - gamma * magSf[faceIndex] /                        \
                            (math.dist(Cf[faceIndex], C[boundaryCellIndex]))
                        
                        # b = - gamma * |Sf| / |db| * psi_b
                        source[cmpt][boundaryCellIndex] +=                      \
                            - gamma * magSf[faceIndex] /                        \
                            (math.dist(Cf[faceIndex], C[boundaryCellIndex]))    \
                            * psiBoundaryValues[cmpt][boundaryIndex]
                
                return source, diag
            
            zebra = fixedValue
                
                
            def empty(mesh, psi, boundaryName, source, diag, gamma):
                
                None
                
                return source, diag
                
                
            def fixedGradient(mesh, psi, boundaryName, source, diag, gamma):
                
                nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
                startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
                
                # Face area magnitudes
                magSf = mesh.geometryData.magSf_
                
                # Number of internal faces
                nInternalFaces = np.size(mesh.connectivityData.neighbour_)
                
                # The value of the gradient
                gradPsi_b = (psi.data.boundaryDefinition_[boundaryName])[1]
                
                noComponents = psi.noComponents_
                
                for cmpt in range(noComponents):
                
                    for faceIndex in range(startFace, startFace + nFaces):
                        
                        boundaryCellIndex = mesh.connectivityData.owner_[faceIndex]
                               
                        boundaryIndex = faceIndex - nInternalFaces

                        # b = - gamma * |Sf| * gradPsi_b
                        source[cmpt][boundaryCellIndex] +=                      \
                             - gamma * magSf[faceIndex] * gradPsi_b[cmpt]
                            
                return source, diag
                
        class volScalarFieldGamma:

            def fixedValue(mesh, psi, boundaryName, source, diag, gamma):
                
                gammaBoundaryValues = gamma.data.boundaryValues_
                
                nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
                startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
                
                # Face area magnitudes
                magSf = mesh.geometryData.magSf_
                # Cell centres
                C = mesh.geometryData.C_
                # Face centres
                Cf = mesh.geometryData.Cf_
                # Values of psi on the boundary
                psiBoundaryValues = psi.data.boundaryValues_
                
                noComponents = psi.noComponents_
                
                # Number of internal faces
                nInternalFaces = np.size(mesh.connectivityData.neighbour_)
                
                # Gamma is a volScalarField, so only one component
                gammaCmpt = 0
                for cmpt in range(noComponents):
                
                    for faceIndex in range(startFace, startFace + nFaces):
                        
                        boundaryCellIndex = mesh.connectivityData.owner_[faceIndex]
                               
                        boundaryIndex = faceIndex - nInternalFaces
                        
                        # ap = - gamma * |Sf| / |db|
                        diag[cmpt][boundaryCellIndex] +=                        \
                            - gammaBoundaryValues[gammaCmpt][boundaryIndex] *   \
                            magSf[faceIndex] /                                  \
                            math.dist(Cf[faceIndex], C[boundaryCellIndex])
                        
                        # b = - gamma * |Sf| / |db| * psi_b
                        source[cmpt][boundaryCellIndex] +=                      \
                            - gammaBoundaryValues[gammaCmpt][boundaryIndex] *   \
                            magSf[faceIndex] /                                  \
                            (math.dist(Cf[faceIndex], C[boundaryCellIndex]))    \
                            * psiBoundaryValues[cmpt][boundaryIndex]
                
                return source, diag
                
                
            def empty(mesh, psi, boundaryName, source, diag, gamma):
                
                None
                
                return source, diag
                
                
            def fixedGradient(mesh, psi, boundaryName, source, diag, gamma):
                
                gammaBoundaryValues = gamma.data.boundaryValues_
                
                nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
                startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
                
                # Face area magnitudes
                magSf = mesh.geometryData.magSf_
                
                # Number of internal faces
                nInternalFaces = np.size(mesh.connectivityData.neighbour_)
                
                # The value of the gradient
                gradPsi_b = (psi.data.boundaryDefinition_[boundaryName])[1]
                
                noComponents = psi.noComponents_
                
                # Gamma is a volScalarField, so only one component
                gammaCmpt = 0
                for cmpt in range(noComponents):
                
                    for faceIndex in range(startFace, startFace + nFaces):
                        
                        boundaryCellIndex = mesh.connectivityData.owner_[faceIndex]
                               
                        boundaryIndex = faceIndex - nInternalFaces

                        # b = - gamma * |Sf| * gradPsi_b
                        source[cmpt][boundaryCellIndex] +=                      \
                             - gammaBoundaryValues[gammaCmpt][boundaryIndex] *  \
                             magSf[faceIndex] * gradPsi_b
                            
                return source, diag
"""
// ************************************************************************* //
"""
