"""
/*---------------------------------------------------------------------------*\
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------

Author
    Robert Anderluh, 2021
    
Description
    Boundary contributions for the fvm divergence operator
    
\*---------------------------------------------------------------------------*/
"""

import numpy as np
import math

class divBoundaryContributions:
    
    class boundaryContributions:
        
        def fixedValue(mesh, psi, boundaryName, source, diag, phi):
            
            nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
            startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
            
            # Values of psi on the boundary
            psiBoundaryValues = psi.data.boundaryValues_
            
            noComponents = psi.noComponents_
            
            # Number of internal faces
            nInternalFaces = np.size(mesh.connectivityData.neighbour_)
            
            for cmpt in range(noComponents):
            
                for faceIndex in range(startFace, startFace + nFaces):
                    
                    boundaryCellIndex = mesh.connectivityData.owner_[faceIndex]
                           
                    boundaryIndex = faceIndex - nInternalFaces
                    
                    # This holds only for a single component phi field!
                    
                    # b = F * psi_b
                    source[cmpt][boundaryCellIndex] +=                          \
                        phi[0][faceIndex] * psiBoundaryValues[cmpt][boundaryIndex]
            
            return source, diag
            
            
        def empty(mesh, psi, boundaryName, source, diag, phi):
            
            None
            
            return source, diag
            
            
        def fixedGradient(mesh, psi, boundaryName, source, diag, phi):
            
            nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
            startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
            
            # Cell centres
            C = mesh.geometryData.C_
            # Face centres
            Cf = mesh.geometryData.Cf_
            
            # Number of internal faces
            nInternalFaces = np.size(mesh.connectivityData.neighbour_)
            
            # The value of the gradient
            gradPsi_b = (psi.data.boundaryDefinition_[boundaryName])[1]
            
            noComponents = psi.noComponents_
            
            for cmpt in range(noComponents):
            
                for faceIndex in range(startFace, startFace + nFaces):
                    
                    boundaryCellIndex = mesh.connectivityData.owner_[faceIndex]
                           
                    boundaryIndex = faceIndex - nInternalFaces
                    
                    # This holds only for a single component phi field!
                    
                    # ap = F
                    diag[cmpt][boundaryCellIndex] +=                            \
                         phi[0][faceIndex]
                    
                    # b = F * |d_b| * gradPsi_b
                    
                    d_b = math.dist(Cf[faceIndex], C[boundaryCellIndex])
                    
                    source[cmpt][boundaryCellIndex] +=                          \
                         phi[0][faceIndex] * d_b * gradPsi_b[cmpt]
                        
            
            return source, diag

"""
// ************************************************************************* //
"""
