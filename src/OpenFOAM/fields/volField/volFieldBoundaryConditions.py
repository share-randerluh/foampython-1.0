"""
/*---------------------------------------------------------------------------*\
 ####                 ####     #   #             |
 #                    #  #     ##  #             | FoamPython
 ##  #### ####  ##### #### # # #   #### #### ### | v1.0
 #   #  # #  #  # # # #     #  # # #  # #  # # # |
 #   #### ##### # # # #    #    #  #  # #### # # |
-------------------------------------------------------------------------------

Author
    Robert Anderluh, 2021
    
Description
    volField class boundary conditions functions
    
\*---------------------------------------------------------------------------*/
"""

import numpy as np
import math

class volFieldBoundaryConditions:
    
    # Functions used to set the appropriate values in the boundaryValues array.
    # This is later used for writing results, calculating gradient, etc.
    class setBoundaryValuesFunctions:
        
        def fixedValue                                                          \
           (mesh, noComponents, cellValues, boundaryValues, boundaryName, boundaryDefinition):

            # Number of internal faces
            nInternalFaces = np.size(mesh.connectivityData.neighbour_)
            
            # Boundary properties
            nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
            startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
            
            # Start and end indices inside the boundaryValues array
            boundaryValuesStart = startFace - nInternalFaces
            boundaryValuesEnd = boundaryValuesStart + nFaces
            
            owner = mesh.connectivityData.owner_
            
            specifiedFixedValue = (boundaryDefinition[boundaryName])[1]
            
            # Assign the appropriate values to the appropriate part of the 
            # boundaryValues array
            for cmpt in range(noComponents):
                
                for faceIndex in range(startFace, startFace + nFaces):
                    
                    boundaryFaceIndex = faceIndex - nInternalFaces
                    
                    boundaryCellIndex = owner[faceIndex]
                    
                    boundaryValues[cmpt][boundaryFaceIndex]                     \
                        = specifiedFixedValue[cmpt]
        
        def zebra                                                       \
           (mesh, noComponents, cellValues, boundaryValues, boundaryName, boundaryDefinition):

            # Number of internal faces
            nInternalFaces = np.size(mesh.connectivityData.neighbour_)
            
            # Boundary properties
            nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
            startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
            
            # Start and end indices inside the boundaryValues array
            boundaryValuesStart = startFace - nInternalFaces
            boundaryValuesEnd = boundaryValuesStart + nFaces
            
            owner = mesh.connectivityData.owner_
            
            # specifiedFixedValue = (boundaryDefinition[boundaryName])[1]['val1']
            
            # Zebra BC definition
            nStripes = (boundaryDefinition[boundaryName])[1]['nStripes']
            val1 = (boundaryDefinition[boundaryName])[1]['val1']
            val2 = (boundaryDefinition[boundaryName])[1]['val2']
            
            # Faces that go into one stripe
            facesPerStripe = int(nFaces / nStripes)
            
            valSwitch = True
            counter = 0
            # Assign the appropriate values to the appropriate part of the 
            # boundaryValues array
            for cmpt in range(noComponents):
                
                for faceIndex in range(startFace, startFace + nFaces):
                    
                    boundaryFaceIndex = faceIndex - nInternalFaces
                    
                    if (counter == facesPerStripe):
                        valSwitch = not valSwitch
                        counter = 0
                    
                    if valSwitch:
                        faceValue = val1
                    else:
                        faceValue = val2
                    
                    counter += 1
                    
                    boundaryCellIndex = owner[faceIndex]
                    
                    boundaryValues[cmpt][boundaryFaceIndex]                     \
                        = faceValue
            
                    
                    
        def empty                                                               \
           (mesh, noComponents, cellValues, boundaryValues, boundaryName, boundaryDefinition):
               
            # Number of internal faces
            nInternalFaces = np.size(mesh.connectivityData.neighbour_)
            
            owner = mesh.connectivityData.owner_
            
            # Boundary properties
            nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
            startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
            
            for cmpt in range(noComponents):
                for faceIndex in range(startFace, startFace + nFaces):
                    
                    boundaryFaceIndex = faceIndex - nInternalFaces
                    
                    boundaryCellIndex = owner[faceIndex]
                    
                    boundaryValues[cmpt][boundaryFaceIndex]                     \
                        = cellValues[cmpt][boundaryCellIndex]
                        
        def fixedGradient                                                       \
           (mesh, noComponents, cellValues, boundaryValues, boundaryName, boundaryDefinition):
               
            # Number of internal faces
            nInternalFaces = np.size(mesh.connectivityData.neighbour_)
            
            owner = mesh.connectivityData.owner_
            
            # Boundary properties
            nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
            startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
            
            # Cell centres
            C = mesh.geometryData.C_
            # Face centres
            Cf = mesh.geometryData.Cf_
            
            specifiedFixedGradient = (boundaryDefinition[boundaryName])[1]
            
            # Assign the appropriate values to the appropriate part of the 
            # boundaryValues array
            for cmpt in range(noComponents):
                for faceIndex in range(startFace, startFace + nFaces):
                    
                    boundaryFaceIndex = faceIndex - nInternalFaces
                    
                    boundaryCellIndex = owner[faceIndex]
                    
                    boundaryValues[cmpt][boundaryFaceIndex]                     \
                        = cellValues[cmpt][boundaryCellIndex] +                 \
                        specifiedFixedGradient[cmpt] *                          \
                        math.dist(Cf[faceIndex], C[boundaryCellIndex])
                        
        def calculated                                                          \
            (mesh, noComponents, cellValues, boundaryValues, boundaryName, boundaryDefinition):
            
            None
    
    # Functions used to set the appropriate values in the boundaryValues array
    # when calculating the gradient of a field.
    class setBoundaryValuesFunctionsGrad:
        
        def fixedValue                                                          \
           (mesh, noComponentsGrad, cellValues, field, boundaryValues, boundaryName, boundaryDefinition):
               
            C = mesh.geometryData.C_
            Cf = mesh.geometryData.Cf_
            
            noComponentsField = field.noComponents_
            
            fieldCellValues = field.data.cellValues_
            fieldBoundaryValues = field.data.boundaryValues_
            
            # Number of internal faces
            nInternalFaces = np.size(mesh.connectivityData.neighbour_)
            
            # Boundary properties
            nFaces = mesh.connectivityData.boundary_[boundaryName]["nFaces"]
            startFace = mesh.connectivityData.boundary_[boundaryName]["startFace"]
            
            owner = mesh.connectivityData.owner_
            
            # For all boundary faces, directly evaluate the gradient using given 
            # field values
            for faceIndex in range(startFace, startFace + nFaces):
                
                boundaryCellIndex = owner[faceIndex]
                
                boundaryFaceIndex = faceIndex - nInternalFaces
                
                # For all grad components
                cmptGrad = 0
                # For all field components
                for cmptField in range(noComponentsField):
                    # For all distance vector components (3)
                    for cmptD in range(3):
                        
                        cellCentre = C[boundaryCellIndex]
                        faceCentre = Cf[boundaryCellIndex]
                        
                        gradFaceCell =                                          \
                            (fieldCellValues[cmptField][boundaryCellIndex]      \
                            -                                                   \
                            fieldBoundaryValues[cmptField][boundaryFaceIndex])
                            
                        distFaceCell =                                          \
                            (C[boundaryCellIndex][cmptD] - Cf[boundaryFaceIndex][cmptD])
                        
                        # If the face centre and the cell centre are at the same
                        # location in a given coordinate axis, copy the value of
                        # the cell gradient to the face
                        if (abs(distFaceCell) < 1e-10):
                            boundaryValues[cmptGrad][boundaryFaceIndex] =       \
                                cellValues[cmptGrad][boundaryCellIndex]
                        else:
                            boundaryValues[cmptGrad][boundaryFaceIndex] =       \
                             (fieldCellValues[cmptField][boundaryCellIndex]     \
                             -                                                  \
                              fieldBoundaryValues[cmptField][boundaryFaceIndex]) / \
                             (C[boundaryCellIndex][cmptD] - Cf[boundaryFaceIndex][cmptD])
                        
                        cmptGrad += 1
        
        zebra = fixedValue
        
        
        # All face gradients are calculated using the pre-existing field values
        # in cells and boundary faces. For that reason, boundary calculation
        # functions are the same for all boundary conditions:
        
        empty = fixedValue
        
        fixedGradient = fixedValue

"""
// ************************************************************************* //
"""
